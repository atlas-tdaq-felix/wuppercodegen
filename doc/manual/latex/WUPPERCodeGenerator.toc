\select@language {english}
\contentsline {chapter}{\numberline {1}Contents}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Installation}{3}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Installation from PyPI using pip}{3}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Scientific Linux and RedHat-based distributions}{3}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Ubuntu}{3}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}Using Virtual Python Environments}{3}{subsection.1.1.4}
\contentsline {section}{\numberline {1.2}User Documentation}{4}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Command Line Interface}{4}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Input Files}{4}{subsection.1.2.2}
\contentsline {subsubsection}{Group}{4}{subsubsection*.3}
\contentsline {subsubsection}{Register and Bitfields}{5}{subsubsection*.4}
\contentsline {subsubsection}{Attributes}{6}{subsubsection*.5}
\contentsline {subsubsection}{Metadata}{7}{subsubsection*.6}
\contentsline {subsubsection}{Example}{8}{subsubsection*.7}
\contentsline {section}{\numberline {1.3}Developer Documentation}{10}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Template Files}{10}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Functions}{11}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Tests}{13}{subsection.1.3.3}
\contentsline {subsection}{\numberline {1.3.4}Filters}{13}{subsection.1.3.4}
\contentsline {subsection}{\numberline {1.3.5}Codes for LaTeX (templates where the output ends in .tex)}{15}{subsection.1.3.5}
\contentsline {subsubsection}{Example}{15}{subsubsection*.81}
\contentsline {section}{\numberline {1.4}Examples}{15}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}A Simple VHDL Template}{15}{subsection.1.4.1}
\contentsline {subsubsection}{Register Description File}{15}{subsubsection*.82}
\contentsline {subsubsection}{Template File}{17}{subsubsection*.83}
\contentsline {subsubsection}{Output File}{18}{subsubsection*.84}
\contentsline {subsection}{\numberline {1.4.2}A List of all Registers}{18}{subsection.1.4.2}
\contentsline {subsubsection}{Register Description File}{18}{subsubsection*.85}
\contentsline {subsubsection}{Template File}{19}{subsubsection*.86}
\contentsline {subsubsection}{Output File}{19}{subsubsection*.87}
\contentsline {chapter}{Python Module Index}{21}{section*.88}
\contentsline {chapter}{Index}{23}{section*.89}
