#!/bin/sh

set -e

REF_YAML_FILE=$1
shift

YAML_FILE=$1
shift

SOURCE_FILE=$1
shift

OPTIONS=$@

INPUT_DIR=input
REF_DIR=ref

DST_FILE=${SOURCE_FILE}${OPTIONS}

wuppercodegen/cli.py --diff ${INPUT_DIR}/${REF_YAML_FILE} ${INPUT_DIR}/${YAML_FILE} ${INPUT_DIR}/${SOURCE_FILE}.template ${DST_FILE}
# remove absolute paths from output
#sed -i "s:${SOURCE_DIR}/::g" ${DST_FILE}
diff ${DST_FILE} ${REF_DIR}
rm ${DST_FILE} || true
