#!/bin/sh

set -e

YAML_FILE=$1
shift

SOURCE_FILE=$1
shift

OPTIONS=$@

INPUT_DIR=input
REF_DIR=ref

#BASENAME=${SOURCE_FILE##*/}
#BASENAME=${BASENAME%.*}
DST_FILE=${SOURCE_FILE}${OPTIONS}

wuppercodegen/cli.py ${INPUT_DIR}/${YAML_FILE} ${INPUT_DIR}/${SOURCE_FILE}.template ${DST_FILE} 2>&1 > ${DST_FILE} || true
# remove absolute paths from output
#sed -i "s:${SOURCE_DIR}/::g" ${DST_FILE}
diff ${DST_FILE} ${REF_DIR}
rm ${DST_FILE} || true
